﻿using System;

namespace Proj.Models
{
    public class GraphicDislocationDensityModel
    {
        public DateTime DateOfGrowth { get; internal set; }
        public double DislocationDensity { get; internal set; }
        public int NumPlate { get; internal set; }
    }
}
