﻿using System;

namespace Proj.Models
{
    public class PlateModel
    {
        public Guid IdPlate { set; get; }
        public double DislocationDensity { set; get; }
        public int NumPlate { set; get; }
        public int NumIngot { set; get; }
        public DateTime DateOfGrowth { set; get; }
        public DateTime DateOfLabAnalysis { set; get; }
        public string OutputDateOfGrowth { get { return DateOfGrowth.ToString("yy-MM-dd"); } }
        public string OutputDateOfLabAnalysis { get { return DateOfLabAnalysis.ToString("yy-MM-dd"); } }
        public int NumOfConsigment { set; get; }
        public string OutputDislocationDensity { get { return DislocationDensity.ToString("E2"); } }
        public Guid? IdIngot { set; get; }
    }
}
