﻿using System;

namespace Proj.Models
{
   public  class StatisticModel
    {
        public int Consigment { set; get; }
        public double? AvgDensity { set; get; }
        public string OutputAvgDensity { get { return (AvgDensity ?? 0).ToString("E"); } }
        public double? AvgMaterial { set; get; }
        public double? AvgDefect { set; get; }
        public double? AvgHallCoefficient { set; get; }
        public string OutputAvgHallCoefficient { get { return (AvgHallCoefficient ?? 0).ToString("E"); } }
        public double? AvgResistivity { set; get; }
        public string OutputAvgResistivity { get { return (AvgResistivity ?? 0).ToString("E"); } }
        public string OutputResistivity { set; get; }
    }
}
