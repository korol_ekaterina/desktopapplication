﻿using System;

namespace Proj.Models
{
    public class DamageModel
    {
        public Guid IdDamage { get; set; }
        public Guid IdIngot { get; set; }
        public Guid IdSituation { get; set; }
    }
}
