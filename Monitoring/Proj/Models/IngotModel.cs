﻿using System;

namespace Proj.Models
{
    public class IngotModel 
    {
        #region private fields
        private int    _numOfConsignment;
        private int    _numOfIngot;
        private double _percDamage;
        private double _minDiameter;
        private double _maxDiameter;
        private double _resistivity;
        private double _hallCoefficient;
        #endregion

        #region Public properties
        public double OutputResistivity { set; get; }
        public Guid IdIngot { get; set; }
        public DateTime DateOfGrowth { get; set; }
        public DateTime DateLabAnalysis { get; set; }
        public string CodOfCriticalSituation1 { get; set; }
        public string CodOfCriticalSituation2 { get; set; }
        public string CodOfUnforeseenSituation { get; set; }
        public string OutputDateOfGrowth { get { return DateOfGrowth.Date.ToString("yy-MM-dd"); } }
        public string OutputDateOfLabAnalysis { get { return DateLabAnalysis.Date.ToString("yy-MM-dd"); } }
        public string OutputResistivityString
        {
            get { return Resistivity.ToString("E"); }
        }

        public double OutputHallCoef { set; get; }

        public string OutputHallCoefString
        {
            get { return HallCoefficient.ToString("E");}
        }

        public int NumOfConsignment
        {
            get { return _numOfConsignment; }
            set
            {
                if (value > 0) _numOfConsignment = value;
                
            }
        }

        public int NumOfIngot
        {
            get { return _numOfIngot; }
            set
            {
                if (value > 0) _numOfIngot = value;
               
            }
        }

        public double PercDamage
        {
            get { return _percDamage; }
            set
            {
                if ((value >= 0) && (value < 100)) _percDamage = value;
                
            }
        }

        public double MinDiameter
        {
            get { return _minDiameter; }
            set
            {
                if (value >= 0) _minDiameter = value;

            }
        }

        public double MaxDiameter
        {
            get { return _maxDiameter; }
            set
            {
                if (value >= 0) _maxDiameter = value;
                
            }
        }

        public double Resistivity
        {
            get { return _resistivity; }
            set
            {
                if (value >= 0) _resistivity = value;
      
            }
        }

        public double HallCoefficient
        {
            get { return _hallCoefficient; }
            set
            {
                if (value >= 0) _hallCoefficient = value;
            
            }
        }
        #endregion
    }
}
