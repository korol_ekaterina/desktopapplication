﻿using System;

namespace Proj.Models
{
  public  class UnforeseenSituationModel
    {
        public string Description { get; set; }
        public string Causes { get; set; }
        public string Solution { get; set; }
        public Guid IdSituation { get; set; }
        public string CodSituation { get; set; }
    }
}
