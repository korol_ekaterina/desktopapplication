﻿
namespace Proj.Interfaces
{
    public interface ICustNavigationService
    {
        void GoBack();
        void NavigateTo(string pageKey);
        void NavigateTo(string pageKey, object parameters);
    }
}
