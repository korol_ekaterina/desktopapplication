﻿using System.Windows.Controls;
using Proj.Interfaces;
using Proj.Views;

namespace Proj
{
    public class CustNavigationService : ICustNavigationService
    {
        public Frame RootFrame { get; set; }
        public ViewModelLocator Locator { get; set; }
        public void GoBack()
        {
            if (RootFrame == null)
                return;
            RootFrame.GoBack();
        }
        public void NavigateTo(string pageKey)
        {
            NavigateTo(pageKey, null);
        }

        public void NavigateTo(string pageKey, object parameters)
        {
            if (RootFrame == null)
                return;
            switch (pageKey)
            {
                case ViewModelLocator.ChartByAvgMaterialConstant:
                    Locator.ChartByAvgMaterial.BuildChartByCurDate(parameters);
                    RootFrame.Navigate(new ChartByAvgMaterial());
                    break;
                case ViewModelLocator.ChartConstant:
                    RootFrame.Navigate(new ChartView());
                    break;
                case ViewModelLocator.ChartByDamageConstant:
                    Locator.ChartByDamage.BuildChartByCurDate(parameters);
                    RootFrame.Navigate(new ChartByDamage());
                    break;
                case ViewModelLocator.ChartDislocDencConstant:
                    Locator.DislocDencityChart.BuildChartByCurDate(parameters);
                    RootFrame.Navigate(new DislocDencityChart());
                    break;
                case ViewModelLocator.MainConstant:
                    RootFrame.Navigate(new MainWindow());
                    break;
                case ViewModelLocator.IngotConstant:
                    RootFrame.Navigate(new IngotView());
                    break;
                case ViewModelLocator.EditIngotConstant:
                    Locator.EditIngotPage.NavigatedParams(parameters);
                    RootFrame.Navigate(new EditIngotView());
                    break;
                case ViewModelLocator.PlateConstant:
                    RootFrame.Navigate(new PlateView());
                    break;
                case ViewModelLocator.EditPlateConstant:
                    Locator.EditPlatePage.NavigatedParams(parameters);
                    RootFrame.Navigate(new EditPlateView());
                    break;
                case ViewModelLocator.SituationConstant:
                    RootFrame.Navigate(new SituationView());
                    break;
                case ViewModelLocator.EditSituationConstant:
                    Locator.EditSituationPage.NavigatedParams(parameters);
                    RootFrame.Navigate(new EditSituationView());
                    break;
                case ViewModelLocator.StatisticConstant:
                    RootFrame.Navigate(new StatisticView());
                    break;
     
            }
        }
       

    }
}
