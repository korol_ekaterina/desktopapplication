﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Proj.Views
{
    /// <summary>
    /// Interaction logic for EditSituationView.xaml
    /// </summary>
    public partial class EditSituationView : Page
    {
        public EditSituationView()
        {
            InitializeComponent();

            grid.Children.Cast<UIElement>()
          .Where(el => el is TextBox).ToList()
          .ForEach(el => Validation.ClearInvalid(((TextBox)el).GetBindingExpression(TextBox.TextProperty)));
        }
    }
}
