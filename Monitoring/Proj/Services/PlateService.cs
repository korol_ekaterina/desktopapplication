﻿using System;
using System.Collections.Generic;
using System.Linq;
using Proj.Models;
using Proj.Services.BaseSingletonClass;

namespace Proj.Services
{
  public  class PlateService : Singleton<PlateService>
  {

        public List<PlateModel> Get()
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionPlateModels(context.Plates).OrderBy(c => c.NumOfConsigment).ThenBy(c=>c.NumIngot).ToList();
            }
        }

        private IQueryable<PlateModel> GetCollectionPlateModels(IQueryable<Plate> col)
        {
            return col
                .Select(item => new PlateModel
                {
                IdPlate = item.IdPlate,
                DislocationDensity = item.DislocationDensity,
                IdIngot = item.IdIngot,
                NumPlate = item.NumPlate,
                NumOfConsigment = item.Ingot.NumConsigment,
                NumIngot = item.Ingot.NumOfIngot,
                DateOfGrowth = item.Ingot.DateOfGrowth,
                DateOfLabAnalysis = item.Ingot.DateLabAnalysis,
                });
        }

        public PlateModel Get(Guid id)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionPlateModels(context.Plates).FirstOrDefault(c => c.IdPlate == id);
            }
        }

        public List<PlateModel> Get(int skip, int take)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionPlateModels(context.Plates).Skip(skip).Take(take).ToList();
            }
        }

        public bool Create(PlateModel dataObject)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                Plate obj = new Plate()
                {
                    IdPlate = Guid.NewGuid(),
                    IdIngot = IngotService.Instance.GetIngot(dataObject.NumOfConsigment, dataObject.NumIngot).IdIngot,
                    NumPlate = dataObject.NumPlate,
                    DislocationDensity = dataObject.DislocationDensity
                };
                context.Plates.Add(obj);
                try
                {
                    if (context.SaveChanges() > 0)
                    {
                        StatisticService statistic = new StatisticService();
                        if (dataObject.IdIngot != null) statistic.ChangeDislocation((Guid) dataObject.IdIngot);
                        return true;
                    }
                   
                }
                catch (Exception)
                {
                    _log.Error("Addition of plate.");
                }
            }
            return false;
        }

        public bool Update(PlateModel dataObject)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var obj = context.Plates.FirstOrDefault(c => c.IdPlate == dataObject.IdPlate);
                if (obj != null)
                {
                    obj.IdIngot = dataObject.IdIngot;
                    obj.NumPlate = dataObject.NumPlate;
                    obj.DislocationDensity = dataObject.DislocationDensity;
                    try
                    {
                        if (context.SaveChanges() > 0)
                        {
                            StatisticService statistic = new StatisticService();
                            if (dataObject.IdIngot != null) statistic.ChangeDislocation((Guid) dataObject.IdIngot);
                            return true;
                        }
                    }
                    catch (Exception)
                    {
                        _log.Error("Updating plate.");
                    }
                }
            }
            return false;
        }

        public bool Delete(PlateModel dataObject)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var obj = context.Plates.Where(c => c.IdPlate == dataObject.IdPlate).Select(c => c).FirstOrDefault();
                context.Plates.Remove(obj);
                try
                {
                   context.SaveChanges();
                }
                catch (Exception)
                {
                    _log.Error("Deleting plate.");
                    return false;
                }
            }
            StatisticService statistic = new StatisticService();
            if (dataObject.IdIngot != null) statistic.ChangeDislocation((Guid)dataObject.IdIngot);
            return true;
        }

        public List<PlateModel> Search(string str)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var col = GetCollectionPlateModels(context.Plates).Where(c => c.DateOfGrowth.ToString().Contains(str)
                    || c.DateOfLabAnalysis.ToString().Contains(str) ||
                    c.DislocationDensity.ToString().Contains(str) || c.NumPlate.ToString().Contains(str) ||
                    c.NumIngot.ToString().Contains(str) || c.NumOfConsigment.ToString().Contains(str));
                return col.ToList();
            }
        }
     
    }
}
