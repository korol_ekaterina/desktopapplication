﻿using System;
using System.Collections.Generic;
using System.Linq;
using Proj.Services.BaseSingletonClass;
using Proj.Models;

namespace Proj.Services
{
    public class IngotService : Singleton<IngotService>
    {
        public List<IngotModel> Get()
        {
            List<IngotModel> collection;
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                collection = GetCollectionIgnotModels(context.Ingots).OrderBy(c => c.NumOfConsignment).ThenBy(c => c.NumOfIngot)
                        .ToList();
            }
            return collection;
        }

        public IngotModel GetIngot(int numConsigment, int numIngot)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionIgnotModels(context.Ingots)
                   .Where(c => c.NumOfConsignment == numConsigment).FirstOrDefault(c => c.NumOfIngot == numIngot);
            }
        }

        private IQueryable<IngotModel> GetCollectionIgnotModels(IQueryable<Ingot> col)
        {
            return col
                .Select(item => new IngotModel
                {
                    IdIngot = item.IdIngot,
                    NumOfConsignment = item.NumConsigment,
                    NumOfIngot = item.NumOfIngot,
                    DateOfGrowth = item.DateOfGrowth,
                    DateLabAnalysis = item.DateLabAnalysis,
                    PercDamage = item.PercDamage,
                    MaxDiameter = item.MaxDiameter,
                    MinDiameter = item.MinDiameter,
                    Resistivity = item.Resistivity,
                    HallCoefficient = item.HallCoefficient,
                    OutputHallCoef = item.HallCoefficient,
                    OutputResistivity = item.Resistivity,
                    //CodOfUnforeseenSituation =
                    //    item.Damages.Where(o => o.IdSituation == item.IdIngot)
                    //        .Select(d => d.UnforeseenSituation.CodSituation).FirstOrDefault(c => c.StartsWith("1")) ?? "-",
                    //CodOfCriticalSituation1 = item.Damages.Where(o => o.IdSituation == item.IdIngot)
                    //        .Select(d => d.UnforeseenSituation.CodSituation).FirstOrDefault(c => c.StartsWith("0")) ?? "-",
                    //CodOfCriticalSituation2 = item.Damages.Where(o => o.IdSituation == item.IdIngot)
                    //    .Select(d => d.UnforeseenSituation.CodSituation).Last(c => c.StartsWith("0")) ?? "-"
                });
        }

        public IngotModel Get(Guid id)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionIgnotModels(context.Ingots.Where(c => c.IdIngot == id)).FirstOrDefault();
            }
        }

        public List<IngotModel> GetByMonth(int month)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionIgnotModels(context.Ingots.Where(c => c.DateOfGrowth.Month == month)).ToList();
            }
        }

        public List<IngotModel> GetByDate(int month, int year)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionIgnotModels(context.Ingots.Where(c => c.DateOfGrowth.Month == month && c.DateOfGrowth.Year==year)).ToList();
            }
        }

        public List<IngotModel> Get(int skip, int take)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionIgnotModels(context.Ingots.OrderBy(c => c.NumConsigment).ThenBy(c => c.NumOfIngot)
                    .Skip(skip).Take(take)).ToList();
            }
        }

        public bool Create(IngotModel dataObject)
        {
            Ingot obj = new Ingot
            {
                IdIngot = dataObject.IdIngot,
                NumConsigment = dataObject.NumOfConsignment,
                NumOfIngot = dataObject.NumOfIngot,
                MaxDiameter = dataObject.MaxDiameter,
                MinDiameter = dataObject.MinDiameter,
                PercDamage = dataObject.PercDamage,
                DateLabAnalysis = dataObject.DateLabAnalysis,
                DateOfGrowth = dataObject.DateOfGrowth,
                Resistivity = dataObject.Resistivity,
                HallCoefficient = dataObject.HallCoefficient
            };

            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                context.Ingots.Add(obj);

                try
                {
                    if (context.SaveChanges() > 0)
                    {
                        StatisticService statisticObj = new StatisticService();
                        statisticObj.ChangeStatistic(dataObject, ActionEnum.Insert); 
                        return true;
                    }
                }
                catch (Exception e)
                {
                    _log.Error("Addition of ingot.");
                }
            }
            return false;
        }

        public bool Update(IngotModel dataObject)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var obj = context.Ingots.SingleOrDefault(c => c.IdIngot == dataObject.IdIngot);
                if (obj != null)
                {
                    obj.NumConsigment = dataObject.NumOfConsignment;
                    obj.NumOfIngot = dataObject.NumOfIngot;
                    obj.MaxDiameter = dataObject.MaxDiameter;
                    obj.MinDiameter = dataObject.MinDiameter;
                    obj.PercDamage = dataObject.PercDamage;
                    obj.DateLabAnalysis = dataObject.DateLabAnalysis;
                    obj.DateOfGrowth = dataObject.DateOfGrowth;
                    obj.Resistivity = dataObject.Resistivity;
                    obj.HallCoefficient = dataObject.HallCoefficient;

                    try
                    {
                        if (context.SaveChanges() >= 0)
                        {
                            StatisticService statisticObj = new StatisticService();
                            statisticObj.ChangeStatistic(dataObject, ActionEnum.Update);
                            return true;
                        }
                    }
                    catch (Exception)
                    {
                        _log.Error("Updating ingot.");
                    }
                }
            }
            return false;
        }

        public bool Delete(IngotModel dataObject)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                try
                {
                    var obj = context.Ingots.FirstOrDefault(c => c.IdIngot == dataObject.IdIngot);
                    context.Ingots.Remove(obj);
                    context.SaveChanges();
                    var ingots = context.Ingots.Where(c => c.NumConsigment == dataObject.NumOfConsignment).Select(c => c);
                    var item = context.ConsigmentStatistics.FirstOrDefault(c => c.NumConsigment == dataObject.NumOfConsignment);
                    if (!ingots.Any()) context.ConsigmentStatistics.Remove(item);
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    _log.Error("Deleting ingot.");
                    return false;
                }
            }
            return true;
        }


        public List<IngotModel> Search(List<IngotModel> list,string str)
        {
                var col = list.Select(c=>c).Where(c => 
                    //c.CodOfCriticalSituation1.Contains(str)
                    //|| c.CodOfCriticalSituation2.Contains(str) || c.CodOfUnforeseenSituation.Contains(str)
                    c.MaxDiameter.ToString().Contains(str)
                    || c.MinDiameter.ToString().Contains(str) ||
                     Convert.ToString(c.DateOfGrowth).Contains(str) 
                     || c.DateLabAnalysis.ToString().Contains(str) ||
                    c.HallCoefficient.ToString().Contains(str) || c.Resistivity.ToString().Contains(str) ||
                    c.NumOfConsignment.ToString().Contains(str) || c.NumOfIngot.ToString().Contains(str)
                    || c.PercDamage.ToString().Contains(str)
                    );
                if (!col.Any()) return list;
                if ((String.IsNullOrEmpty(str))) return Get();
                return col.ToList();
        }

        public List<IngotModel> GetIngotsByNumConsigment(int numConsigment)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionIgnotModels(context.Ingots).Where(c => c.NumOfConsignment == numConsigment).ToList();
            }
        }
        public List<int> GetNumIngotsByNumConsigment(int numConsigment)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionIgnotModels(context.Ingots).Where(c => c.NumOfConsignment == numConsigment).Select(c=>c.NumOfIngot).Distinct().OrderBy(c=>c).ToList();
            }
        }
        public List<int> GetUniqueNumConsigmentsOfIngots()
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionIgnotModels(context.Ingots).Select(c => c.NumOfConsignment).Distinct().ToList();
            }
        }

        public List<GraphicDislocationDensityModel> GetGraphicDislocationDensityModel(int monthToSelect, int year)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var collection = context.Ingots.Join(context.Plates, i => i.IdIngot, p => p.IdIngot,
                    (i, p) => new GraphicDislocationDensityModel
                    {
                        DateOfGrowth = i.DateOfGrowth,
                        NumPlate = p.NumPlate,
                        DislocationDensity = p.DislocationDensity
                    })
                .Where(c => c.DateOfGrowth.Month == monthToSelect && c.DateOfGrowth.Year == year).ToList();
                return collection;
            }
        }
    }
}
