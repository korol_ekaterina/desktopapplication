﻿using log4net;
using Proj.Models;
using System;
using System.Linq;
using Proj.Services.BaseSingletonClass;
using System.Collections.Generic;

namespace Proj.Services
{
   public class DamageService : Singleton<DamageService>
    {

        public List<double> GetDislocationOfPlates(Guid idIngot)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return context.Plates.Where(c => c.Ingot.IdIngot == idIngot).Select(c => c.DislocationDensity)
                    .ToList();
            }
        }

        public void Create(DamageModel dataObject)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var criticalSituation = 
                    context.Damages.Any(c => c.IdIngot == dataObject.IdIngot && 
                                            c.UnforeseenSituation.CodSituation.StartsWith("0"));
                var emergencySituation =
                    context.Damages.Count(c => c.IdIngot == dataObject.IdIngot
                                            && c.UnforeseenSituation.CodSituation.StartsWith("1"));
                try
                {
                    // Limits the number of records. Emergency situations can not be more than one on the ingot.
                    if (criticalSituation)
                        throw new Exception("An attempt to add more than one emergency situation.");
                    // Limits the number of records. Critical situations can not be more than two on the ingot.
                    if (emergencySituation >= 2)
                        throw new Exception("An attempt to add more than two critical situations.");
                    Damage obj = new Damage
                    {
                        IdDamage = Guid.NewGuid(),
                        IdSituation = dataObject.IdSituation,
                        IdIngot = dataObject.IdIngot
                    };
                    context.Damages.Add(obj);
                    if (context.SaveChanges() <= 0 ) throw new Exception("Addition of Damage object.");
                }
                catch (Exception exception)
                {
                    _log.Error(exception.Message);
                }
           }
        }
    }
}
