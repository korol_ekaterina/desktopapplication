﻿using log4net;
using System;

namespace Proj.Services.BaseSingletonClass
{
    public abstract class Singleton<T> where T : Singleton<T>, new()
    {
        protected ILog _log = LogManager.GetLogger(typeof(T));

        static T _uniqueService;
        private static object syncRoot = new Object();
        public static T Instance
        {
            get
            {
                if (_uniqueService == null)
                {
                    lock (syncRoot)
                    {
                        if (_uniqueService == null)
                            _uniqueService = new T();
                    }
                }
                return _uniqueService;
            }
        }
    }
}
