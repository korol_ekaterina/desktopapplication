﻿using System;
using System.Collections.Generic;
using System.Linq;
using Proj.Services.BaseSingletonClass;
using Proj.Models;

namespace Proj.Services
{
   public class UnforeseenSituationService : Singleton<UnforeseenSituationService>
    {

        public List<UnforeseenSituationModel> Get()
            {
                using (MonitoringDataEntities context = new MonitoringDataEntities())
                {
                    return GetCollectionUnforeseenSituationModels(context.UnforeseenSituations).ToList();
                }
            }

        private IQueryable<UnforeseenSituationModel> GetCollectionUnforeseenSituationModels(IQueryable<UnforeseenSituation> col)
        {
            return col.Select(item => new UnforeseenSituationModel
            {
                IdSituation = item.IdSituation,
                Causes = item.Causes,
                Description = item.Description,
                CodSituation = item.CodSituation
            });
        }

        public UnforeseenSituationModel GetSituationById(Guid id)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionUnforeseenSituationModels(context.UnforeseenSituations)
                    .FirstOrDefault(c => c.IdSituation == id);
            }
        }

        public bool Create(UnforeseenSituationModel dataObject)
            {

                UnforeseenSituation obj = new UnforeseenSituation()
                {
                    IdSituation = dataObject.IdSituation,
                    Description = dataObject.Description,
                    Causes = dataObject.Causes,
                    CodSituation = dataObject.CodSituation,
                    Solution = dataObject.Solution
                };
                using (MonitoringDataEntities context = new MonitoringDataEntities())
                {
                try
                    {
                        context.UnforeseenSituations.Add(obj);
                        if (context.SaveChanges() > 0) return true;
                    }
                    catch (Exception)
                    {
                        _log.Error("Addition of unforeseen situation.");
                    }
                }
            return false;
            }

            public bool Update(UnforeseenSituationModel dataObject)
            {
                using (MonitoringDataEntities context = new MonitoringDataEntities())
                {
                    try
                    {
                        var obj =
                            context.UnforeseenSituations.SingleOrDefault(c => c.IdSituation == dataObject.IdSituation);
                        if (obj != null)
                        {
                            obj.Description = dataObject.Description;
                            obj.Causes = dataObject.Causes;
                            obj.CodSituation = dataObject.CodSituation;
                            obj.Solution = dataObject.Solution;
                        }
                        if (context.SaveChanges() >= 0) return true;
                    }
                    catch (Exception)
                    {
                        _log.Error("Updating unforeseen situation."); 
                    }
                }
                return false;
            }

            public bool Delete(UnforeseenSituationModel dataObject)
            {
                using (MonitoringDataEntities context = new MonitoringDataEntities())
                {
                var obj = context.UnforeseenSituations.SingleOrDefault(c => c.IdSituation == dataObject.IdSituation);
                    try
                    {
                        context.UnforeseenSituations.Remove(obj);
                        context.SaveChanges();
                    }
                    catch (Exception)
                    {
                        _log.Error("Deleting unforeseen situation.");
                        return false;
                    }
                }
                return true;
            }

            public List<UnforeseenSituationModel> Search(string str)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var col = GetCollectionUnforeseenSituationModels(context.UnforeseenSituations)
                    .Where(c => c.Causes.Contains(str) || c.Description.Contains(str) ||
                       c.CodSituation.Contains(str));
                return col.ToList();
            }
            }
    }
}
