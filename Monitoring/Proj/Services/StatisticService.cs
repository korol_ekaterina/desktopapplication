﻿using Proj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Proj.Services.BaseSingletonClass;
using log4net;

namespace Proj.Services
{
    public class StatisticService : Singleton<StatisticService>
    {
        #region constants
        private const double MinPermissibleResistivity = 1e-04;
        private const double MaxPermissibleResistivity = 1e+10;
        private const double MaxPermissibleDislocation = 70000;
        private const double MaxPermissiblePercOfDamagedMaterial = 65;
        private const double MinHallCoefficient = 1e-4;
        private const double MaxHallCoefficient = 1e-2;
        private const double MinPermissibleDiametrOfMinCrystal = 39;
        private const double MaxPermissibleDiametrOfMinCrystal = 40.5;
        private const double MinPermissibleDiametrOfAvgCrystal = 50.3;
        private const double MaxPermissibleDiametrOfAvgCrystal = 61.3;
        private const double MinPermissibleDiametrOfMaxCrystal = 75.7;
        private const double MaxPermissibleDiametrOfMaxCrystal = 76.7;
        #endregion constants

        public double PartDamaged(List<IngotModel> ingotColl)
        {
            double partDamaged = 0;
            DamageService damageService = DamageService.Instance;
            foreach (var el in ingotColl)
            {
                int count = 0;
                var plateDisloc = damageService.GetDislocationOfPlates(el.IdIngot);
                if (plateDisloc.Any())
                {
                    foreach (var plateDislVal in plateDisloc)
                    {
                        if (plateDislVal > MaxPermissibleDislocation) count++;
                    }
                }

                if ((el.Resistivity < MinPermissibleResistivity) || (!PermissibleDiametr(el.MaxDiameter)) || (el.Resistivity > MaxPermissibleResistivity) ||
                     (el.PercDamage > MaxPermissiblePercOfDamagedMaterial) || (el.MaxDiameter <= el.MinDiameter) ||
                     (count == 3) || (el.HallCoefficient < MinHallCoefficient) || (el.HallCoefficient > MaxHallCoefficient))
                    partDamaged++;

                else if (el.PercDamage != 0 && el.PercDamage <= MaxPermissiblePercOfDamagedMaterial)
                {
                    partDamaged = partDamaged + el.PercDamage / 100;
                }
            }

            return partDamaged;
        }

        public Boolean PermissibleDiametr(double diametr)
        {
            return (((diametr >= MinPermissibleDiametrOfMinCrystal) && (diametr <= MaxPermissibleDiametrOfMinCrystal)) ||
                ((diametr >= MinPermissibleDiametrOfAvgCrystal) && (diametr <= MaxPermissibleDiametrOfAvgCrystal))
            || ((diametr >= MinPermissibleDiametrOfMaxCrystal) && (diametr <= MaxPermissibleDiametrOfMaxCrystal)));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="action"></param>
        public void ChangeStatistic(IngotModel dataObject, ActionEnum action)
        {
            IngotService ingotService = IngotService.Instance;
            var ingotColl = ingotService.GetIngotsByNumConsigment(dataObject.NumOfConsignment);

            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var statisticByConsigment =
                    context.ConsigmentStatistics.Where(c => c.NumConsigment == dataObject.NumOfConsignment);
                try
                {
                    if (ingotColl.Any())
                    {
                        ConsigmentStatistic item = statisticByConsigment.FirstOrDefault();
                        if (item != null)
                        {
                            item.AvgResistivity = ingotColl.Select(c => c.Resistivity).Average();
                            item.AvgHallKoef = ingotColl.Select(c => c.HallCoefficient).Average();

                            item.AvgDislocDensity =
                                context.Ingots.Where(c => c.NumConsigment == dataObject.NumOfConsignment)
                                    .Select(c => c.Plates.Average(p => p.DislocationDensity)).FirstOrDefault();

                            item.Damage = Math.Round(PartDamaged(ingotColl) /
                                                     ingotService.GetIngotsByNumConsigment(dataObject.NumOfConsignment)
                                                         .Count() * 100, 2);
                        }
                        context.SaveChanges();
                    }
                    else if (ingotColl.Count() == 1 && action != ActionEnum.Delete)
                    {
                        ConsigmentStatistic item = statisticByConsigment.FirstOrDefault();
                        if (item != null)
                        {
                            item.AvgResistivity = ingotColl.Select(c => c.Resistivity).Average();
                            item.AvgHallKoef = ingotColl.Select(c => c.HallCoefficient).Average();
                            item.AvgDislocDensity = context.Ingots.Where(c => c.NumConsigment == dataObject.NumOfConsignment)
                                .Select(i => i.Plates.Select(c => c.DislocationDensity).Average()).FirstOrDefault();
                            item.Damage = Math.Round(PartDamaged(ingotColl) /
                                                     ingotService.GetIngotsByNumConsigment(dataObject.NumOfConsignment)
                                                         .Count() * 100, 2);
                        }
                        context.SaveChanges();
                    }

                    if (!ingotColl.Any() && action == ActionEnum.Delete)
                    {
                        context.ConsigmentStatistics.Remove(statisticByConsigment.FirstOrDefault());
                        context.SaveChanges();
                    }
                }
                catch (Exception)
                {
                    _log.Error("Error in method ChangeStatistic.");
                }
            }
        }

        public void ChangeDislocation(Guid idIngot)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var numConsigment = context.Ingots.Where(c => c.IdIngot == idIngot).Select(c => c.NumConsigment).FirstOrDefault();
                var record = context.ConsigmentStatistics.Where(c => c.NumConsigment == numConsigment).Select(c => c).FirstOrDefault();
                if (record != null)
                    record.AvgDislocDensity = context.Ingots.Where(c => c.NumConsigment == numConsigment)
                        .Select(i => i.Plates.Average(p => p.DislocationDensity)).FirstOrDefault();
                context.SaveChanges();
            }
        }

        private IQueryable<StatisticModel> GetCollectionStatisticModels(IQueryable<ConsigmentStatistic> collection)
        {
            return collection.Select(item => new StatisticModel
            {
                AvgDefect = item.Damage,
                AvgDensity = item.AvgDislocDensity,
                AvgHallCoefficient = item.AvgHallKoef,
                AvgResistivity = item.AvgResistivity,
                Consigment = item.NumConsigment
            });
        }

        public List<StatisticModel> GetStatistic()
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionStatisticModels(context.ConsigmentStatistics).ToList();
            }
        }

        public bool AnyGetStatisticByNumConsigment(int numConsigment)
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionStatisticModels(context.ConsigmentStatistics)
                    .Any(c => c.Consigment == numConsigment);
            }
        }

        public List<int> GetNumConsigments()
        {
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                return GetCollectionStatisticModels(context.ConsigmentStatistics)
                    .Select(c => c.Consigment).ToList();
            }
        }

        public bool Create()
        {
            ConsigmentStatistic obj = new ConsigmentStatistic()
            {

            };

            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                context.ConsigmentStatistics.Add(obj);

                try
                {
                    if (context.SaveChanges() > 0) return true;
                }
                catch (Exception e)
                {
                    _log.Error("Addition of statistic." + e.Message);
                  //  throw;
                }
            }
            return false;
        }

        public List<StatisticModel> Search(string str)
        {
            var searchCol = GetStatistic().Where(c => c.OutputAvgDensity.Contains(str) || c.OutputAvgHallCoefficient.Contains(str)
                || c.OutputAvgResistivity.Contains(str) || c.Consigment.ToString().Contains(str)).Select(c => c);
            return searchCol.ToList();
        }
    }

    public enum ActionEnum
    {
        Insert,
        Update,
        Delete,
        Navigate
    }

}
