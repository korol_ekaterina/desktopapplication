﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Proj.ViewModels;

namespace Proj
{
    public class ViewModelLocator
    {
        public const string MainConstant = "Main";
        public const string IngotConstant = "IngotPage";
        public const string EditIngotConstant = "EditIngotPage";
        public const string PlateConstant = "PlatePage";
        public const string EditPlateConstant = "EditPlatePage";
        public const string SituationConstant = "SituationPage";
        public const string EditSituationConstant = "EditSituationPage";
        public const string StatisticConstant = "StatisticPage";
        public const string ChartConstant = "ChartPage";
        public const string ChartDislocDencConstant = "DislocDencityChart";
        public const string ChartByDamageConstant = "ChartByDamage";
        public const string ChartByAvgMaterialConstant = "ChartByAvgMaterial";
        public ViewModelLocator()
        {
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<IngotViewModel>();
            SimpleIoc.Default.Register<EditIngotViewModel>();
            SimpleIoc.Default.Register<PlateViewModel>();
            SimpleIoc.Default.Register<EditPlateViewModel>();
            SimpleIoc.Default.Register<SituationViewModel>();
            SimpleIoc.Default.Register<EditSituationViewModel>();
            SimpleIoc.Default.Register<StatisticViewModel>();
            SimpleIoc.Default.Register<ChartViewModel>();
            SimpleIoc.Default.Register<DislocDencityViewModel>();
            SimpleIoc.Default.Register<ChartByDamageViewModel>();
            SimpleIoc.Default.Register<ChartByAvgMaterialViewModel>();
        }

        public MainViewModel Main { get { return SimpleIoc.Default.GetInstance<MainViewModel>(); } }
        public IngotViewModel IngotPage { get { return SimpleIoc.Default.GetInstance<IngotViewModel>(); } }
        public EditIngotViewModel EditIngotPage { get { return SimpleIoc.Default.GetInstance<EditIngotViewModel>(); } }
        public PlateViewModel PlatePage { get { return SimpleIoc.Default.GetInstance<PlateViewModel>(); } }
        public EditPlateViewModel EditPlatePage { get { return SimpleIoc.Default.GetInstance<EditPlateViewModel>(); } }
        public SituationViewModel SituationPage { get { return SimpleIoc.Default.GetInstance<SituationViewModel>(); } }
        public StatisticViewModel StatisticPage { get { return SimpleIoc.Default.GetInstance<StatisticViewModel>(); } }
        public EditSituationViewModel EditSituationPage { get { return SimpleIoc.Default.GetInstance<EditSituationViewModel>(); } }
        public ChartViewModel ChartPage { get { return SimpleIoc.Default.GetInstance<ChartViewModel>(); } }
        public DislocDencityViewModel DislocDencityChart { get { return SimpleIoc.Default.GetInstance<DislocDencityViewModel>(); } }
        public ChartByDamageViewModel ChartByDamage { get { return SimpleIoc.Default.GetInstance<ChartByDamageViewModel>(); } }
        public ChartByAvgMaterialViewModel ChartByAvgMaterial { get { return SimpleIoc.Default.GetInstance<ChartByAvgMaterialViewModel>(); } }
    }
}
