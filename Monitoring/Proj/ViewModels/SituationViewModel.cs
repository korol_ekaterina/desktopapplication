﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using GalaSoft.MvvmLight;
using Proj.Services;
using Proj.Interfaces;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Proj.Models;

namespace Proj.ViewModels
{
    public class SituationViewModel : ViewModelBase
    {
        #region Bindable properties
        private ObservableCollection<UnforeseenSituationModel> _situations;
        public ObservableCollection<UnforeseenSituationModel> Situations
        {
            get
            {
                return _situations ??
                       (_situations =
                           new ObservableCollection<UnforeseenSituationModel>(UnforeseenSituationService.Instance.Get()));
            }
            set
            {
                _situations = value;
                RaisePropertyChanged(() => Situations);
            }
        }

        private UnforeseenSituationModel _selectedSituation;
        public UnforeseenSituationModel SelectedSituation
        {
            get { return _selectedSituation; }
            set
            {
                if (_selectedSituation == value)
                    return;
                _selectedSituation = value;
                RaisePropertyChanged(() => SelectedSituation);
            }
        }

        #endregion

        private ICustNavigationService _navigationService;

        public SituationViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;
            Messenger.Default.Register<EditSituationViewModelParameters>(this, (par) =>
            {
                switch (par.Action)
                {
                    case ActionEnum.Insert:
                        Situations.Add(par.Situation);
                        break;

                    case ActionEnum.Update:
                        var s = Situations.FirstOrDefault(c => c.IdSituation == par.Situation.IdSituation);
                        s.Causes = par.Situation.Causes;
                        s.CodSituation = par.Situation.CodSituation;
                        s.Description = par.Situation.Description;
                        break;

                    default:
                        break;
                }

            });
        }

        #region commands
        private RelayCommand _addCommand;
        private RelayCommand _updateCommand;
        private RelayCommand _deleteCommand;

        public RelayCommand AddSituationCommand
        {
            get
            {
                return _addCommand ?? (_addCommand = new RelayCommand(() =>
                    _navigationService.NavigateTo(ViewModelLocator.EditSituationConstant,
                        new EditSituationViewModelParameters
                        {
                            Action = ActionEnum.Insert
                        })));
            }
        }

        public RelayCommand UpdateSituationCommand
        {
            get
            {
                return _updateCommand ?? (_updateCommand = new RelayCommand(() =>
                {
                    if (SelectedSituation != null)
                    {
                        _navigationService.NavigateTo(ViewModelLocator.EditSituationConstant,
                            new EditSituationViewModelParameters
                            {
                                Situation = SelectedSituation,
                                Action = ActionEnum.Update
                            });
                    }
                }));
            }
        }

        public RelayCommand DeleteSituationCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand = new RelayCommand(() =>
                {
                    UnforeseenSituationService.Instance.Delete(SelectedSituation);
                    Situations.Remove(SelectedSituation);
                }));
            }
        }
        #endregion

    }
}
