﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Proj.Services;
using Proj.Interfaces;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using log4net;
using Proj.Models;

namespace Proj.ViewModels
{
    public class EditIngotViewModel : BaseViewModel, IDataErrorInfo
    {
        #region Bindable properties
        private string _numOfIngot;
        private string _dateOfGrowth;
        private string _dateLabAnalysis;
        private string _minDiametr;
        private string _maxDiametr;
        private string _hallCoef;
        private string _resistivity;
        private string _percDamage;

        public string NumOfIngot
        {
            get { return _numOfIngot; }
            set
            {
                _numOfIngot = value;
                RaisePropertyChanged(() => NumOfIngot);
            }
        }
        public string DateOfGrowth
        {
            get { return _dateOfGrowth; }
            set
            {
                _dateOfGrowth = value;
                RaisePropertyChanged(() => DateOfGrowth);
            }
        }
        public string DateLabAnalysis
        {
            get { return _dateLabAnalysis; }
            set
            {
                _dateLabAnalysis = value;
                RaisePropertyChanged(() => DateLabAnalysis);
            }
        }
        public String MinDiametr
        {
            get { return _minDiametr; }
            set
            {
                _minDiametr = value;
                RaisePropertyChanged(() => MinDiametr);
            }
        }
        public string MaxDiametr
        {
            get { return _maxDiametr; }
            set
            {
                _maxDiametr = value;
                RaisePropertyChanged(() => MaxDiametr);
            }
        }
        public string HallCoeficient
        {
            get { return _hallCoef; }
            set
            {
                _hallCoef = value;
                RaisePropertyChanged(() => HallCoeficient);
            }
        }
        public string Resistivity
        {
            get { return _resistivity; }
            set
            {
                _resistivity = value;
                RaisePropertyChanged(() => Resistivity);
            }
        }
        public string PercDamage
        {
            get { return _percDamage; }
            set
            {
                _percDamage = value;
                RaisePropertyChanged(() => PercDamage);
            }
        }

        private bool _canSave;
        public bool CanSave
        {
            get { return _canSave; }
            set
            {
                _canSave = value;
                RaisePropertyChanged(() => CanSave);
            }
        }

        # region validation

        private static readonly string[] ValidationProperties =
        {
       "NumOfIngot","DateOfGrowth","DateLabAnalysis",
       "MinDiametr","MaxDiametr","PercDamage","Resistivity"
        };
        public string this[string columnName]
        {
            get
            {
                CanSave = IsValid();
                return GetValidationError(columnName);
            }
        }

        string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)
            {
                case "NumOfIngot":
                    int res;
                    if (String.IsNullOrEmpty(NumOfIngot))
                    {
                        error = Properties.Resources.ErrorRequiredValue;
                    }
                    else if (!Int32.TryParse(NumOfIngot, out res))
                    {
                        error = Properties.Resources.ErrorNumericValue;
                    }
                    else if (res < 0)
                    {
                        error = Properties.Resources.ErrorLessZero;
                    }
                    break;
                case "MinDiametr":
                    error = CheckDoubleValue(MinDiametr);
                    break;
                case "MaxDiametr":
                    error = CheckDoubleValue(MaxDiametr);
                    break;
                case "HallCoeficient":
                    error = CheckDoubleValue(HallCoeficient);
                    break;
                case "Resistivity":
                    error = CheckDoubleValue(Resistivity);
                    break;
                case "PercDamage":
                    double res5;
                    if (String.IsNullOrEmpty(PercDamage))
                    {
                        error = Properties.Resources.ErrorRequiredValue;
                    }
                    else if (!Double.TryParse(PercDamage, out res5))
                    {
                        error = Properties.Resources.ErrorNumericValue;
                    }
                    else if ((res5 < 0) || (res5 > 100))
                    {
                        error = Properties.Resources.ErrorBetween0_100;
                    }
                    break;
            }
            return error;
        }
        public string Error
        {
            get { return this[null]; }
        }

        public bool IsValid()
        {
            foreach (string property in ValidationProperties)
            {
                if (GetValidationError(property) != null)
                    return false;
            }
            return true;
        }

        private string CheckDoubleValue(string value)
        {
            double result;
            if (String.IsNullOrEmpty(value))
            {
                return Properties.Resources.ErrorRequiredValue;
            }
            else if (!Double.TryParse(value, out result))
            {
                return Properties.Resources.ErrorNumericValue;
            }
            else if (result < 0)
            {
                return Properties.Resources.ErrorLessZero;
            }
            else return null;
        }
        # endregion

        private ObservableCollection<int> _consigmentsAvailable;
        public ObservableCollection<int> ConsigmentsAvailable
        {
            get
            {
                return _consigmentsAvailable ??
                       (_consigmentsAvailable =
                           new ObservableCollection<int>(StatisticService.Instance.GetNumConsigments()));
            }
            set
            {
                _consigmentsAvailable = value;
                RaisePropertyChanged(() => ConsigmentsAvailable);
            }
        }
        private int _selectedConsigment;
        public int SelectedConsigment
        {
            get { return _selectedConsigment; }
            set
            {
                if (_selectedConsigment != value)
                {
                    _selectedConsigment = value;
                    RaisePropertyChanged(() => SelectedConsigment);
                }
            }
        }

        #endregion

        private ICustNavigationService _navigationService;
        private Guid _id;
        private ActionEnum _action;
        private ILog _log = LogManager.GetLogger(typeof(EditIngotViewModel));
        public EditIngotViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;

        }
        public IngotModel CreateIngotModel()
        {
            try
            {
                IngotModel ingot = new IngotModel()
                {
                    IdIngot = _id,
                    DateLabAnalysis = Convert.ToDateTime(DateLabAnalysis),
                    DateOfGrowth = Convert.ToDateTime(DateOfGrowth),
                    HallCoefficient = Convert.ToDouble(HallCoeficient),
                    MinDiameter = Convert.ToDouble(MinDiametr),
                    MaxDiameter = Convert.ToDouble(MaxDiametr),
                    NumOfIngot = Convert.ToInt32(NumOfIngot),
                    NumOfConsignment = ConsigmentsAvailable
                        .FirstOrDefault(c => c == SelectedConsigment),
                    PercDamage = Convert.ToDouble(PercDamage),
                    Resistivity = Convert.ToDouble(Resistivity)
                };
                return ingot;
            }
            catch (Exception e)
            {
                _log.Error("Error of creation IngotModel in EditIngotViewModel class." + e.InnerException);
            }
            return new IngotModel();
        }

        #region commands
        private RelayCommand _saveCommand;
        public RelayCommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(SaveCommandExecute)); }
        }
        private void SaveCommandExecute()
        {
            IngotModel ingot = CreateIngotModel();
            if (_action == ActionEnum.Insert)
            {
                if (!IngotService.Instance.Create(ingot)) return;
            }
            else
            {
                if (!IngotService.Instance.Update(ingot)) return;
            }
            _navigationService.NavigateTo(ViewModelLocator.IngotConstant);
            Messenger.Default.Send(new NotificationMessage("Refresh"));
        }

        private RelayCommand _addConsigmentCommand;
        public RelayCommand AddConsigmentCommand
        {
            get
            {
                return _addConsigmentCommand ?? (_addConsigmentCommand = new RelayCommand(() =>
                {
                    if (StatisticService.Instance.Create()) ConsigmentsAvailable.Add(ConsigmentsAvailable.Last() + 1);
                }));
            }
        }

        private RelayCommand _cancelCommand;
        public RelayCommand CancelCommand
        {
            get { return _cancelCommand ?? (_cancelCommand = new RelayCommand(CancelCommandExecute)); }
        }

        private void CancelCommandExecute()
        {
            _navigationService.GoBack();
        }

        #endregion
        public override void NavigatedParams(object parameters)
        {
            if (parameters is EditIngotViewModelParameters)
            {
                EditIngotViewModelParameters par = (EditIngotViewModelParameters)parameters;
                if (par.Action == ActionEnum.Update)
                {
                    _id = par.Ingot.IdIngot;
                    DateLabAnalysis = par.Ingot.DateLabAnalysis.ToString();
                    DateOfGrowth = par.Ingot.DateOfGrowth.ToString();
                    HallCoeficient = par.Ingot.HallCoefficient.ToString();
                    MinDiametr = par.Ingot.MinDiameter.ToString();
                    MaxDiametr = par.Ingot.MaxDiameter.ToString();
                    NumOfIngot = par.Ingot.NumOfIngot.ToString();
                    Resistivity = par.Ingot.Resistivity.ToString();
                    SelectedConsigment = ConsigmentsAvailable.FirstOrDefault(c => c == par.Ingot.NumOfConsignment);
                    PercDamage = par.Ingot.PercDamage.ToString();
                    _action = ActionEnum.Update;
                }
                else
                {
                    _id = Guid.NewGuid();
                    DateLabAnalysis = DateTime.Now.ToString();
                    DateOfGrowth = DateTime.Now.ToString();
                    HallCoeficient = "0";
                    MinDiametr = "0";
                    MaxDiametr = "0";
                    Resistivity = "0";
                    SelectedConsigment = ConsigmentsAvailable.FirstOrDefault();
                    NumOfIngot = "0";
                    PercDamage = "0";
                    _action = ActionEnum.Insert;
                }
            }
            else
            {
                _navigationService.GoBack();
            }
        }
    }

    public class EditIngotViewModelParameters
    {
        public IngotModel Ingot { get; set; }
        public ActionEnum Action { get; set; }

    }
}

