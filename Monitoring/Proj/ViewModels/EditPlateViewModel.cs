﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Proj.Models;
using Proj.Services;
using Proj.Interfaces;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Proj.ViewModels
{
    public class EditPlateViewModel : BaseViewModel, IDataErrorInfo
    {
        #region bindable properties

        private string _plateDencity;
        public string PlateDencity
        {
            get { return _plateDencity; }
            set
            {
                _plateDencity = value;
                RaisePropertyChanged(() => PlateDencity);
            }
        }
        private bool _canSave;
        public bool CanSave
        {
            get { return _canSave; }
            set
            {
                _canSave = value;
                RaisePropertyChanged(() => CanSave);
            }
        }

        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                switch (columnName)
                {
                    case "PlateDencity":
                        double result;
                        if (String.IsNullOrEmpty(PlateDencity))
                        {
                            CanSave = false;
                            return Properties.Resources.ErrorRequiredValue;
                        }
                        else if (!Double.TryParse(PlateDencity, out result))
                        {
                            CanSave = false;
                            return Properties.Resources.ErrorNumericValue;
                        }
                        else if (result < 0)
                        {
                            CanSave = false;
                            return Properties.Resources.ErrorLessZero;
                        }
                        break;
                }
                CanSave = error == String.Empty;
                return error;
            }
        }

        public string Error
        {
            get { return this[string.Empty]; }
        }

        private ObservableCollection<int> _consigmentsAvailable;
        public ObservableCollection<int> ConsigmentsAvailable
        {
            get
            {
                return _consigmentsAvailable ??
                       (_consigmentsAvailable =
                           new ObservableCollection<int>(IngotService.Instance.GetUniqueNumConsigmentsOfIngots()));
            }
            set
            {
                _consigmentsAvailable = value;
                RaisePropertyChanged(() => ConsigmentsAvailable);
            }
        }

        private int _selectedConsigment;
        public int SelectedConsigment
        {
            get { return _selectedConsigment; }
            set
            {
                if (_selectedConsigment != value)
                {
                    _selectedConsigment = value;
                    RaisePropertyChanged(() => SelectedConsigment);
                }
            }
        }

        private ObservableCollection<int> _numIngotsAvailable;
        public ObservableCollection<int> NumIngotsAvailable
        {
            get { return _numIngotsAvailable; }
            set
            {
                _numIngotsAvailable = value;
                RaisePropertyChanged(() => NumIngotsAvailable);
            }
        }

        private int _selectedNumIngot;
        public int SelectedNumIngot
        {
            get { return _selectedNumIngot; }
            set
            {
                _selectedNumIngot = value;
                RaisePropertyChanged(() => SelectedNumIngot);
            }
        }

        int[] numPlates = { 1, 2, 3 };
        private ObservableCollection<int> _numPlatesAvailable;
        public ObservableCollection<int> NumPlatesAvailable
        {
            get { return _numPlatesAvailable ?? (_numPlatesAvailable = new ObservableCollection<int>(numPlates)); }
            set
            {
                _numPlatesAvailable = value;
                RaisePropertyChanged(() => NumPlatesAvailable);
            }
        }

        private int _selectedNumPlate;
        public int SelectedNumPlate
        {
            get { return _selectedNumPlate; }
            set
            {
                if (_selectedNumPlate != value)
                {
                    _selectedNumPlate = value;
                    RaisePropertyChanged(() => SelectedNumPlate);
                }
            }
        }
        #endregion

        private DateTime _dateOfGrowth;
        private DateTime _dateOfLabAnalysis;
        private Guid? _idIngot;
        private Guid _id;
        private ActionEnum _action;

        private ICustNavigationService _navigationService;

        public EditPlateViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        private PlateModel CreatePlateModel()
        {
            PlateModel plate = new PlateModel()
            {
                IdPlate = _id,
                DislocationDensity = Convert.ToDouble(PlateDencity),
                NumIngot = SelectedNumIngot,
                NumPlate = SelectedNumPlate,
                NumOfConsigment = SelectedConsigment,
                IdIngot = _idIngot
            };
            return plate;
        }

        #region commands
        private RelayCommand _saveCommand;
        public RelayCommand SavePlateCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(SaveCommandExecute)); }
        }
        private void SaveCommandExecute()
        {
            PlateModel plate = CreatePlateModel();
            if (_action == ActionEnum.Insert)
            {
                if (SelectedNumIngot != 0)
                {
                    if (!PlateService.Instance.Create(plate)) return;
                }
                else return;
            }
            else
            {
                plate.DateOfGrowth = _dateOfGrowth;
                plate.DateOfLabAnalysis = _dateOfLabAnalysis;
                if (!PlateService.Instance.Update(plate)) return;
            }
            _navigationService.NavigateTo(ViewModelLocator.PlateConstant);
            Messenger.Default.Send(new NotificationMessage("Refresh"));
        }

        private RelayCommand _ingotsByNumConsigmentCommand;
        public RelayCommand IngotsByNumConsigmentCommand
        {
            get
            {
                return _ingotsByNumConsigmentCommand ?? (_ingotsByNumConsigmentCommand = new RelayCommand(IngotsByNumConsigmentCommandExecute));
            }
        }

        private void IngotsByNumConsigmentCommandExecute()
        {
            NumIngotsAvailable = new ObservableCollection<int>(IngotService.Instance
                .GetNumIngotsByNumConsigment(SelectedConsigment));
            SelectedNumIngot = NumIngotsAvailable.FirstOrDefault();
        }

        private RelayCommand _cancelCommand;
        public RelayCommand CancelCommand
        {
            get
            {
                return _cancelCommand ?? (_cancelCommand = new RelayCommand(() =>
                    _navigationService.GoBack()
                    ));
            }
        }

        #endregion
        public override void NavigatedParams(object parameters)
        {
            if (parameters is EditPlateViewModelParameters)
            {
                EditPlateViewModelParameters par = (EditPlateViewModelParameters)parameters;
                if (par.Action == ActionEnum.Update)
                {
                    _id = par.Plate.IdPlate;
                    PlateDencity = par.Plate.DislocationDensity.ToString();
                    SelectedConsigment = par.Plate.NumOfConsigment;
                    NumIngotsAvailable = new ObservableCollection<int>(IngotService.Instance.GetNumIngotsByNumConsigment(SelectedConsigment));
                    SelectedNumIngot = par.Plate.NumIngot;
                    SelectedNumPlate = par.Plate.NumPlate;
                    _dateOfGrowth = par.Plate.DateOfGrowth;
                    _dateOfLabAnalysis = par.Plate.DateOfLabAnalysis;
                    _idIngot = par.Plate.IdIngot;
                    _action = ActionEnum.Update;
                }
                else
                {
                    _id = Guid.NewGuid();
                    PlateDencity = "0";
                    SelectedConsigment = ConsigmentsAvailable.First();
                    SelectedNumPlate = NumPlatesAvailable.First();
                    NumIngotsAvailable = new ObservableCollection<int>(IngotService.Instance.GetNumIngotsByNumConsigment(SelectedConsigment));
                    SelectedNumIngot = NumIngotsAvailable.FirstOrDefault();
                    _action = ActionEnum.Insert;
                }
            }
            else
            {
                _navigationService.GoBack();
            }
        }
    }

    public class EditPlateViewModelParameters
    {
        public PlateModel Plate { get; set; }
        public ActionEnum Action { get; set; }

    }
}
