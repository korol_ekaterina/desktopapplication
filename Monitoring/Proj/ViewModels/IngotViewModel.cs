﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using GalaSoft.MvvmLight;
using Proj.Models;
using Proj.Services;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Proj.Interfaces;

namespace Proj.ViewModels
{
    public class IngotViewModel : ViewModelBase
    {
        #region Bindable properties
        private ObservableCollection<IngotModel> _ingots;
        public ObservableCollection<IngotModel> Ingots
        {
            get { return _ingots ?? (_ingots = new ObservableCollection<IngotModel>(IngotService.Instance.Get())); }
            set
            {
                _ingots = value;
                RaisePropertyChanged(() => Ingots);
                IngotsView = CollectionViewSource.GetDefaultView(Ingots);
                RaisePropertyChanged(() => IngotsView);
            }
        }

        private IngotModel _selectedIngot;
        public IngotModel SelectedIngot
        {
            get { return _selectedIngot; }
            set
            {
                _selectedIngot = value;
                RaisePropertyChanged(() => SelectedIngot);
            }
        }
        public ICollectionView IngotsView { get; set; }

        private string _ingotSearch;
        public string IngotSearch
        {
            get { return _ingotSearch; }
            set
            {
                _ingotSearch = value;            
                IngotsView.Filter = CustomerFilter;
                RaisePropertyChanged(() => IngotSearch);
                IngotsView.Refresh();
            }
        }
        #endregion

        private IngotService ingotService = IngotService.Instance;
        private ICustNavigationService _navigationService;
        public IngotViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;
            Messenger.Default.Register<NotificationMessage>(this, (message) =>
            {
                if (message.Notification == "Refresh")
                {
                    Ingots = new ObservableCollection<IngotModel>(IngotService.Instance.Get());
                }
            });

            IngotsView = CollectionViewSource.GetDefaultView(Ingots);
        }
        private bool CustomerFilter(object item)
        {
            IngotModel ingot = item as IngotModel;
            if (ingot!=null) 
                return (ingot.MaxDiameter.ToString().Contains(IngotSearch)
                || ingot.MinDiameter.ToString().Contains(IngotSearch) 
                ||ingot.DateOfGrowth.ToString().Contains(IngotSearch)
                || ingot.DateLabAnalysis.ToString().Contains(IngotSearch) 
                ||ingot.OutputHallCoefString.Contains(IngotSearch) 
                ||ingot.OutputResistivityString.Contains(IngotSearch) 
                ||ingot.NumOfConsignment.ToString().Contains(IngotSearch) 
                || ingot.NumOfIngot.ToString().Contains(IngotSearch)
                || ingot.PercDamage.ToString().Contains(IngotSearch));
            return  false;
        }

        #region commands

        private RelayCommand _addCommand;
        private RelayCommand _updateCommand;
        private RelayCommand _deleteCommand;
        public RelayCommand AddIngotCommand
        {
            get { return _addCommand ?? (_addCommand = new RelayCommand(()=>
                 _navigationService.NavigateTo(ViewModelLocator.EditIngotConstant, new EditIngotViewModelParameters
            {
                Action = ActionEnum.Insert
            })            
                )); }
        }

        public RelayCommand UpdateIngotCommand
        {
            get
            {
                return _updateCommand ?? (_updateCommand = new RelayCommand(() =>
                {
                    if (SelectedIngot != null)
                    {
                        _navigationService.NavigateTo(ViewModelLocator.EditIngotConstant,
                            new EditIngotViewModelParameters
                            {
                                Ingot = SelectedIngot,
                                Action = ActionEnum.Update
                            });
                    }
                }));
            }
        }

        public RelayCommand DeleteIngotCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand = new RelayCommand(() =>
                {
                    ingotService.Delete(SelectedIngot);
                    Ingots.Remove(SelectedIngot);
                }));
            }
        }
  
        #endregion
    }
}
