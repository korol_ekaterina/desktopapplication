﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using log4net;
using Proj.Services;
using Proj.Interfaces;

namespace Proj.ViewModels
{
    public class ChartByAvgMaterialViewModel : ViewModelBase
    {
        # region bindable properties
        private string _error;
        public string Error
        {
            get { return _error; }
            set
            {
                _error = value;
                RaisePropertyChanged(() => Error);
            }
        }

        private DateTime _date;
        private string _selectedDate;
        public string SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                _selectedDate = value;
                RaisePropertyChanged(() => SelectedDate);
                _date = Convert.ToDateTime(SelectedDate);
            }
        }

        private ObservableCollection<PointMaterial> _avgDamaged;

        public ObservableCollection<PointMaterial> AverageDamaged
        {
            get { return _avgDamaged; }
            set
            {
                _avgDamaged = value;
                RaisePropertyChanged(() => AverageDamaged);
            }
        }

        private ObservableCollection<PointMaterial> _materialByShifts;

        public ObservableCollection<PointMaterial> MaterialByShifts
        {
            get { return _materialByShifts; }
            set
            {
                _materialByShifts = value;
                RaisePropertyChanged(() => MaterialByShifts);
            }
        }

        # endregion

        private ICustNavigationService _navigationService;
        public ChartByAvgMaterialViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;
        }
        public void BuildChartByCurDate(object curDate)
        {
            SelectedDate = Convert.ToDateTime(curDate).ToString();
            GetGraph(Convert.ToDateTime(curDate));
        }

        StatisticService statistic = StatisticService.Instance;
        private ILog _log = LogManager.GetLogger(typeof(ChartByAvgMaterialViewModel));
        public void GetGraph(DateTime date)
        {
            IngotService ingotService = IngotService.Instance;
            List<PointMaterial> materialByShifts = new List<PointMaterial>();
            List<PointMaterial>  avgDamaged = new List<PointMaterial>();
            var collection = ingotService.GetByDate(date.Month, date.Year);
            var dates = collection.Select(c => c.DateOfGrowth).Distinct();
            if (collection.Count() < 5)
            {
                Error = Properties.Resources.ChartErrorMessage;
            }
            else
            {
                Error = String.Empty;
                materialByShifts.AddRange(from item in dates
                    let productsByCurDate = collection.Where(c => c.DateOfGrowth == item).ToList()
                    select new PointMaterial
                    {
                        Shift = item,
                        CountMaterial =
                            Math.Round(100 - 100*statistic.PartDamaged(productsByCurDate)/productsByCurDate.Count(), 2)
                    });

                var avgDamage = Math.Round(100 - 100*statistic.PartDamaged(collection)/collection.Count(), 2);

                avgDamaged = new List<PointMaterial>()
                {

                    new PointMaterial {Shift = dates.Min(), CountMaterial = avgDamage},
                    new PointMaterial {Shift = dates.Max(), CountMaterial = avgDamage}
                };            
            }
            AverageDamaged = new ObservableCollection<PointMaterial>(avgDamaged);
            MaterialByShifts = new ObservableCollection<PointMaterial>(materialByShifts);
        }

        private RelayCommand _buildChartCommand;
        private ICommand _returnCommand;

        public RelayCommand BuildChartCommand
        {
            get { return _buildChartCommand ?? (_buildChartCommand = new RelayCommand(BuildChartCommandExecute)); }
        }

        private void BuildChartCommandExecute()
        {
            try
            {
                GetGraph(_date);
            }
            catch (Exception)
            {
                _log.Error("Building of graphic by dislocation dencity.");
            }
        }

        public ICommand ReturnCommand
        {
            get
            {
                return _returnCommand ?? (_returnCommand = new RelayCommand(() =>
                {
                    _navigationService.NavigateTo(ViewModelLocator.ChartConstant);
                }));
            }
        }

        public class PointMaterial
        {
            public double CountMaterial { get; set; }
            public DateTime Shift { get; set; }
        }
    }
}
