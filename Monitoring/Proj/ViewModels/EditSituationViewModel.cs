﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Proj.Models;
using Proj.Services;
using Proj.Interfaces;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Proj.ViewModels
{
    public class EditSituationViewModel : BaseViewModel, IDataErrorInfo
    {

        #region bindable properties

        private string _description;
        private string _codSituation;
        private string _causes;
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged(() => Description);
            }
        }

        public string CodSituation
        {
            get { return _codSituation; }
            set
            {
                _codSituation = value;
                RaisePropertyChanged(() => CodSituation);
            }
        }
        public string Causes
        {
            get { return _causes; }
            set
            {
                _causes = value;
                RaisePropertyChanged(() => Causes);
            }
        }

        private bool _canSave;
        public bool CanSave
        {
            get { return _canSave; }
            set
            {
                _canSave = value;
                RaisePropertyChanged(() => CanSave);
            }
        }
        # region vaidation
        private static readonly string[] ValidationProperties =
        {
       "CodSituation","Causes","Description"
        };
        public string this[string columnName]
        {
            get
            {
                CanSave = IsValid();
                return GetValidationError(columnName);
            }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)
            {
                case "CodSituation":
                    if (String.IsNullOrEmpty(CodSituation))
                    {
                        error = Properties.Resources.ErrorRequiredValue;
                    }
                    break;
                case "Causes":
                    if (String.IsNullOrEmpty(Causes))
                    {
                        error = Properties.Resources.ErrorRequiredValue;
                    }
                    break;
                case "Description":
                    if (String.IsNullOrEmpty(Description))
                    {
                        error = Properties.Resources.ErrorRequiredValue;
                    }
                    break;
            }
            return error;
        }

        public string Error
        {
            get { return this[null]; }
        }

        public bool IsValid()
        {
            foreach (string property in ValidationProperties)
            {
                if (GetValidationError(property) != null)
                    return false;
            }
            return true;
        }
        #endregion
        #endregion
        private ICustNavigationService _navigationService;
        private Guid _id;
        private ActionEnum _action;

        public EditSituationViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        private UnforeseenSituationModel CreateSituation()
        {
            var situation = new UnforeseenSituationModel()
            {
                IdSituation = _id,
                Description = Description,
                Causes = Causes,
                CodSituation = CodSituation
            };
            return situation;
        }

        #region commands
        private RelayCommand _saveCommand;
        public RelayCommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(SaveCommandExecute)); }
        }

        private void SaveCommandExecute()
        {
            UnforeseenSituationModel situation = CreateSituation();
            if (_action == ActionEnum.Insert)
            {
                if (!UnforeseenSituationService.Instance.Create(situation)) return;
            }
            else
            {
                if (!UnforeseenSituationService.Instance.Update(situation)) return;
            }
            _navigationService.NavigateTo(ViewModelLocator.SituationConstant);
            Messenger.Default.Send(new EditSituationViewModelParameters()
            {
                Situation = situation,
                Action = _action
            });
        }

        private RelayCommand _cancelCommand;
        public RelayCommand CancelCommand
        {
            get { return _cancelCommand ?? (_cancelCommand = new RelayCommand(CancelCommandExecute)); }
        }

        private void CancelCommandExecute()
        {
            _navigationService.GoBack();
        }

        #endregion

        public override void NavigatedParams(object parameters)
        {
            if (parameters is EditSituationViewModelParameters)
            {
                EditSituationViewModelParameters par = (EditSituationViewModelParameters)parameters;
                if (par.Action == ActionEnum.Update)
                {
                    _id = par.Situation.IdSituation;
                    Description = par.Situation.Description;
                    CodSituation = par.Situation.CodSituation;
                    Causes = par.Situation.Causes;
                    _action = ActionEnum.Update;
                }
                else
                {
                    _id = Guid.NewGuid();
                    Description = "";
                    CodSituation = "";
                    Causes = "";
                    _action = ActionEnum.Insert;
                }
            }
            else
            {
                _navigationService.GoBack();
            }
        }
    }

    public class EditSituationViewModelParameters
    {
        public UnforeseenSituationModel Situation { get; set; }
        public ActionEnum Action { get; set; }

    }
}

