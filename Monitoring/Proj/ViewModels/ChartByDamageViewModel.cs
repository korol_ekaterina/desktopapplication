﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using log4net;
using Proj.Services;
using Proj.Interfaces;
using GalaSoft.MvvmLight.Command;

namespace Proj.ViewModels
{
    public class ChartByDamageViewModel : ViewModelBase
    {
        # region bindable properties
        private string _error;
        public string Error
        {
            get { return _error; }
            set
            {
                _error = value;
                RaisePropertyChanged(() => Error);
            }
        }

        private DateTime _date;
        private string _selectedDate;
        public string SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                _selectedDate = value;
                RaisePropertyChanged(() => SelectedDate);
                _date = Convert.ToDateTime(SelectedDate);
            }
        }

        private ObservableCollection<PointDamaged> _avgDamaged;
        public ObservableCollection<PointDamaged> AverageDamaged
        {
            get { return _avgDamaged; }
            set
            {
                _avgDamaged = value;
                RaisePropertyChanged(() => AverageDamaged);
            }
        }

        private ObservableCollection<PointDamaged> _damageByShifts;

        public ObservableCollection<PointDamaged> DamageByShifts
        {
            get { return _damageByShifts; }
            set
            {
                _damageByShifts = value;
                RaisePropertyChanged(() => DamageByShifts);
            }
        }

        # endregion

        private StatisticService statistic = new StatisticService();
        private ILog _log = LogManager.GetLogger(typeof(ChartByDamageViewModel));
        private ICustNavigationService _navigationService;

        public ChartByDamageViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public void BuildChartByCurDate(object curDate)
        {
            SelectedDate = Convert.ToDateTime(curDate).ToString();
            GetGraph(Convert.ToDateTime(curDate));
        }

        public void GetGraph(DateTime date)
        {
            List<PointDamaged> damageByShifts = new List<PointDamaged>();
            List<PointDamaged> avgDamaged = new List<PointDamaged>();
            IngotService ingotService = IngotService.Instance;
            var collection = ingotService.GetByDate(date.Month, date.Year);
            var dates = collection.Select(c => c.DateOfGrowth).Distinct();
            if (collection.Count() < 5)
            {
                Error = Properties.Resources.ChartErrorMessage;
            }
            else
            {
                Error = String.Empty;
                damageByShifts.AddRange(from item in dates
                                        let productsByCurDate = collection.Where(c => c.DateOfGrowth == item).Select(c => c).ToList()
                                        select new PointDamaged
                                        {
                                            Shift = item,
                                            PercDamaged =
                                                Math.Round(100 * statistic.PartDamaged(productsByCurDate) / productsByCurDate.Count(), 2)
                                        });
                var avgDamage = Math.Round(100 * statistic.PartDamaged(collection.ToList()) / collection.Count(), 2);
                avgDamaged = new List<PointDamaged>()
                {

                    new PointDamaged {Shift = dates.Min(), PercDamaged = avgDamage},
                    new PointDamaged {Shift = dates.Max(), PercDamaged = avgDamage}
                };             
            }
            DamageByShifts = new ObservableCollection<PointDamaged>(damageByShifts);
            AverageDamaged = new ObservableCollection<PointDamaged>(avgDamaged);
        }

        private RelayCommand _buildChartCommand;
        private ICommand _returnCommand;

        public RelayCommand BuildChartCommand
        {
            get { return _buildChartCommand ?? (_buildChartCommand = new RelayCommand(BuildChartCommandExecute)); }
        }

        private void BuildChartCommandExecute()
        {
            try
            {
                GetGraph(_date);
            }
            catch (Exception)
            {
                _log.Error("Building of graphic by dislocation dencity.");
            }
        }

        public ICommand ReturnCommand
        {
            get
            {
                return _returnCommand ?? (_returnCommand = new RelayCommand(() =>
                {
                    _navigationService.NavigateTo(ViewModelLocator.ChartConstant);
                }));
            }
        }

        public class PointDamaged
        {
            public double PercDamaged { get; set; }
            public DateTime Shift { get; set; }

        }
    }
}
