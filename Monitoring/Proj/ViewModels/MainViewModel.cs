﻿using GalaSoft.MvvmLight;
using Proj.Interfaces;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Proj.Views;

namespace Proj.ViewModels
{
   public class MainViewModel : ViewModelBase
    {
        private ICustNavigationService _navigationService;

        public MainViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;
        }
        private RelayCommand _navigationCommand;
        private RelayCommand _navigationPlatesCommand;
        private RelayCommand _navigationSituationsCommand;
        private RelayCommand _navigationStatisticCommand ;
        private RelayCommand _navigationChartCommand;
        public RelayCommand NavigationToIngotsCommand { get { return _navigationCommand ?? (_navigationCommand = new RelayCommand(NavigationCommandExecute)); } }

        private void NavigationCommandExecute()
        {
            _navigationService.NavigateTo(ViewModelLocator.IngotConstant);
        }

        public RelayCommand NavigationToPlatesCommand { get { return _navigationPlatesCommand ?? (_navigationPlatesCommand = new RelayCommand(NavigationPlatesCommandExecute)); } }

        private void NavigationPlatesCommandExecute()
        {
            _navigationService.NavigateTo(ViewModelLocator.PlateConstant);
        }
        public RelayCommand NavigationToSituationsCommand { get { return _navigationSituationsCommand ?? (_navigationSituationsCommand = new RelayCommand(NavigationSituationsCommandExecute)); } }

        private void NavigationSituationsCommandExecute()
        {
            _navigationService.NavigateTo(ViewModelLocator.SituationConstant);
        }
        public RelayCommand NavigationToStatisticCommand { get { return _navigationStatisticCommand ?? (_navigationStatisticCommand = new RelayCommand(NavigationStatisticCommandExecute)); } }

        private void NavigationStatisticCommandExecute()
        {
            _navigationService.NavigateTo(ViewModelLocator.StatisticConstant);
        }

       public RelayCommand NavigationToChartCommand { get { return _navigationChartCommand ?? (_navigationChartCommand = new RelayCommand(NavigationChartCommandExecute)); } }

        private void NavigationChartCommandExecute()
        {
            _navigationService.NavigateTo(ViewModelLocator.ChartConstant);
        }

    }
}
