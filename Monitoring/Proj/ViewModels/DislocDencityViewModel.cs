﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using log4net;
using Proj.Services;
using Proj.Interfaces;
using GalaSoft.MvvmLight.Command;

namespace Proj.ViewModels
{
    public class DislocDencityViewModel : ViewModelBase
    {
        # region bindable properties
        private string _error;
        public string Error
        {
            get { return _error; }
            set
            {
                _error = value;
                RaisePropertyChanged(() => Error);
            }
        }

        private string _selectedDate;
        private DateTime _date;
        public string SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                _selectedDate = value;
                RaisePropertyChanged(() => SelectedDate);
                _date = Convert.ToDateTime(SelectedDate);
            }
        }

        private ObservableCollection<PointDisloc> _avgPoints;

        public ObservableCollection<PointDisloc> AverageDislocationDensity
        {
            get { return _avgPoints; }
            set
            {
                _avgPoints = value;
                RaisePropertyChanged(() => AverageDislocationDensity);
            }
        }

        private ObservableCollection<PointDisloc> _topPoints;

        public ObservableCollection<PointDisloc> TopDislocationDensity
        {
            get { return _topPoints; }
            set
            {
                _topPoints = value;
                RaisePropertyChanged(() => TopDislocationDensity);
            }
        }

        private ObservableCollection<PointDisloc> _bottomPoints;

        public ObservableCollection<PointDisloc> BottomDislocationDensity
        {
            get { return _bottomPoints; }
            set
            {
                _bottomPoints = value;
                RaisePropertyChanged(() => BottomDislocationDensity);
            }
        }

        # endregion

        private ICustNavigationService _navigationService;

        public DislocDencityViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public void BuildChartByCurDate(object curDate)
        {
            SelectedDate = Convert.ToDateTime(curDate).ToString();
            GetGraph(Convert.ToDateTime(curDate));
        }

        private ILog _log = LogManager.GetLogger(typeof(DislocDencityViewModel));
        private IngotService _ingotService = IngotService.Instance;

        public void GetGraph(DateTime date)
        {
            List<PointDisloc> bottomPlateDensity = new List<PointDisloc>();
            List<PointDisloc> topPlateDensity = new List<PointDisloc>();
            List<PointDisloc> averageDislocationDensity = new List<PointDisloc>();

            var collection = _ingotService.GetGraphicDislocationDensityModel(date.Month, date.Year);

            if (collection.Count < 5)
            {
                Error = Properties.Resources.ChartErrorMessage;
            }
            else
            {
                Error = String.Empty;
                var dates = collection.Select(c => c.DateOfGrowth).Distinct().OrderBy(c => c);
                foreach (var item in dates)
                {
                    PointDisloc minPoint = new PointDisloc
                    {
                        Shift = item,
                        DislocationDensity =
                            collection.Where(c => c.DateOfGrowth == item).Min(c => c.DislocationDensity)
                    };
                    bottomPlateDensity.Add(minPoint);
                    PointDisloc maxPoint = new PointDisloc
                    {
                        Shift = item,
                        DislocationDensity =
                            collection.Where(c => c.DateOfGrowth == item).Max(c => c.DislocationDensity)
                    };
                    topPlateDensity.Add(maxPoint);
                    PointDisloc avgPoint = new PointDisloc
                    {
                        Shift = item,
                        DislocationDensity =
                            collection.Where(c => c.DateOfGrowth == item).Average(c => c.DislocationDensity)
                    };
                    averageDislocationDensity.Add(avgPoint);
                }

            }
            AverageDislocationDensity = new ObservableCollection<PointDisloc>(averageDislocationDensity);
            BottomDislocationDensity = new ObservableCollection<PointDisloc>(bottomPlateDensity);
            TopDislocationDensity = new ObservableCollection<PointDisloc>(topPlateDensity);
        }

        public class PointDisloc
        {
            public double DislocationDensity { get; set; }
            public DateTime Shift { get; set; }

        }

        private RelayCommand _buildChartCommand;
        private RelayCommand _returnCommand;

        public RelayCommand BuildChartCommand
        {
            get { return _buildChartCommand ?? (_buildChartCommand = new RelayCommand(BuildChartCommandExecute)); }
        }

        private void BuildChartCommandExecute()
        {
            try
            {
                GetGraph(_date);
            }
            catch (Exception)
            {
                _log.Error("Building of graphic by dislocation dencity.");
            }
        }

        public RelayCommand ReturnCommand
        {
            get
            {
                return _returnCommand ?? (_returnCommand = new RelayCommand(() =>
                {
                    _navigationService.NavigateTo(ViewModelLocator.ChartConstant);
                }));
            }
        }
    }
}
