﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using GalaSoft.MvvmLight;
using Proj.Models;
using Proj.Services;
using Proj.Interfaces;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Proj.ViewModels
{
    public class PlateViewModel : ViewModelBase
    {
        #region Bindable properties
        private ObservableCollection<PlateModel> _plates;
        public ObservableCollection<PlateModel> Plates
        {
            get { return _plates ?? (_plates = new ObservableCollection<PlateModel>(PlateService.Instance.Get())); }
            set
            {
                _plates = value;
                RaisePropertyChanged(() => Plates);
                PlatesView = CollectionViewSource.GetDefaultView(Plates);
                RaisePropertyChanged(() => PlatesView);
            }
        }

        private PlateModel _selectedPlate;
        public PlateModel SelectedPlate
        {
            get { return _selectedPlate; }
            set
            {
                _selectedPlate = value;
                RaisePropertyChanged(() => SelectedPlate);
            }
        }

        public ICollectionView PlatesView { get; set; }

        private string _plateSearch;
        public string PlateSearch
        {
            get { return _plateSearch; }
            set
            {
                _plateSearch = value;
                PlatesView.Filter = CustomerFilter;
                RaisePropertyChanged(() => PlateSearch);
                PlatesView.Refresh();
            }
        }

        #endregion

        private ICustNavigationService _navigationService;

        public PlateViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;
            Messenger.Default.Register<NotificationMessage>(this, (message) =>
            {
                if (message.Notification == "Refresh")
                {
                    Plates = new ObservableCollection<PlateModel>(PlateService.Instance.Get());
                }
            });

            PlatesView = CollectionViewSource.GetDefaultView(Plates);
        }

        private bool CustomerFilter(object item)
        {
            PlateModel plate = item as PlateModel;
            if (plate != null)
                return (plate.DateOfGrowth.ToString().Contains(PlateSearch)
                    || plate.DateOfLabAnalysis.ToString().Contains(PlateSearch)
                    || plate.OutputDislocationDensity.Contains(PlateSearch)
                    || plate.NumPlate.ToString().Contains(PlateSearch)
                    || plate.NumIngot.ToString().Contains(PlateSearch)
                    || plate.NumOfConsigment.ToString().Contains(PlateSearch));
            return false;
        }

        # region commands
        private RelayCommand _addPlateCommand;

        public RelayCommand AddPlateCommand
        {
            get
            {
                return _addPlateCommand ?? (_addPlateCommand = new RelayCommand(() =>
                    _navigationService.NavigateTo(ViewModelLocator.EditPlateConstant, new EditPlateViewModelParameters
                    {
                        Action = ActionEnum.Insert
                    })));
            }
        }


        private RelayCommand _updatePlateCommand;
        public RelayCommand UpdatePlateCommand
        {
            get
            {
                return _updatePlateCommand ?? (_updatePlateCommand = new RelayCommand(() =>
                {
                    if (SelectedPlate != null)
                    {
                        _navigationService.NavigateTo(ViewModelLocator.EditPlateConstant,
                            new EditPlateViewModelParameters
                            {
                                Plate = SelectedPlate,
                                Action = ActionEnum.Update
                            });
                    }
                }));
            }
        }

        private RelayCommand _deletePlateCommand;
        public RelayCommand DeletePlateCommand
        {
            get
            {
                return _deletePlateCommand ?? (_deletePlateCommand = new RelayCommand(() =>
                {
                    PlateService.Instance.Delete(SelectedPlate);
                    Plates.Remove(SelectedPlate);
                }));
            }
        }
        # endregion
    }
}
