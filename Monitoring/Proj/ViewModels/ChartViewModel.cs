﻿using System;
using System.Windows.Input;
using Proj.Interfaces;
using GalaSoft.MvvmLight.Command;

namespace Proj.ViewModels
{
    public class ChartViewModel
    {
        private ICustNavigationService _navigationService;
        DateTime curDate = DateTime.Now;
        public ChartViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;
        }
        private RelayCommand _navigationChartCommand;
        public RelayCommand NavigationToDislocDencChartCommand { get { return _navigationChartCommand ?? (_navigationChartCommand = new RelayCommand(NavigationCommandExecute)); } }
        private void NavigationCommandExecute()
        {
            _navigationService.NavigateTo(ViewModelLocator.ChartDislocDencConstant, curDate);
        }
        private RelayCommand _navigationToChartByDamageCommand;
        public ICommand NavigationToChartByDamageCommand
        {
            get
            {
                return _navigationToChartByDamageCommand ?? (_navigationToChartByDamageCommand = new RelayCommand(() =>
                {
                    _navigationService.NavigateTo(ViewModelLocator.ChartByDamageConstant, curDate);
                }));
            }
        }
        private RelayCommand _navigationToChartByAvgMaterialCommand;
        public ICommand NavigationToChartByAvgMaterialCommand
        {
            get
            {
                return _navigationToChartByAvgMaterialCommand ?? (_navigationToChartByAvgMaterialCommand = new RelayCommand(() =>
                {
                    _navigationService.NavigateTo(ViewModelLocator.ChartByAvgMaterialConstant, curDate);
                }));
            }
        }
    }
}
