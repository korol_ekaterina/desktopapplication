﻿using GalaSoft.MvvmLight;

namespace Proj.ViewModels
{
    public abstract class BaseViewModel:ViewModelBase
    {
        public abstract void NavigatedParams(object parameters);
    }
}
