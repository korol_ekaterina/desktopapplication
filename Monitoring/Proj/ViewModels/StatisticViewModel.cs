﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using GalaSoft.MvvmLight;
using Proj;
using Proj.Models;
using Proj.Services;
using Proj.Interfaces;

namespace Proj.ViewModels
{
    public class StatisticViewModel : ViewModelBase
    {
        #region Bindable properties
        private ObservableCollection<StatisticModel> _statistics;
        public ObservableCollection<StatisticModel> Statistics
        {
            get {
                return _statistics ??
                       (_statistics = new ObservableCollection<StatisticModel>(StatisticService.Instance.GetStatistic()));
            }
            set
            {
                _statistics = value;
                RaisePropertyChanged(() => Statistics);
            }
        }

        private UnforeseenSituationModel _selectedSituation;
        public UnforeseenSituationModel SelectedSituation
        {
            get { return _selectedSituation; }
            set
            {
                _selectedSituation = value;
                RaisePropertyChanged(() => SelectedSituation);
            }
        }

        #endregion

        private ICustNavigationService _navigationService;

        public StatisticViewModel(ICustNavigationService navigationService)
        {
            _navigationService = navigationService;
        }
    }
}
